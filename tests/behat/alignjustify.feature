@editor @editor_atto @atto @atto_alignjustify
Feature: Atto align justify text
  To format text with align justify in Atto, I need to use the align buttons.

  @javascript
  Scenario: Justify align some text
    Given I log in as "admin"
    And I open my profile in edit mode
    And I set the field "Description" to "<p>Fascism</p>"
    And I click on "Show more buttons" "button"
    And I select the text in the "Description" Atto editor
    When I click on "Align Justify" "button"
    And I press "Update profile"
    And I follow "Preferences" in the user menu
    And I follow "Editor preferences"
    And I set the field "Text editor" to "Plain text area"
    And I press "Save changes"
    And I follow "Edit profile"
    Then I should see "style=\"text-align:justify;\""

