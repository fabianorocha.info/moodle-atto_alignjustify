<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Atto text editor justify align plugin lib.
 *
 * @package    atto_alignjustify
 * @copyright  2018 Middag Technology
 * @author     Michael Menezes <michael@michaelmeneses.com.br>
 * @author     Fabiano Rocha <fabianorocha.info@gmail.com>
 * @author     Moisés Cassimiro <grytor@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Initialise the strings required for JS.
 * 
 * @return void
 */
function atto_alignjustify_strings_for_js() {
    global $PAGE;
    $PAGE->requires->strings_for_js(array('justify'), 'atto_alignjustify');
}
