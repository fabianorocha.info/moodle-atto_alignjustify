// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    atto_alignjustify
 * @copyright  2018 Middag Technology
 * @author     Michael Menezes <michael@michaelmeneses.com.br>
 * @author     Fabiano Rocha <fabianorocha.info@gmail.com>
 * @author     Moisés Cassimiro <grytor@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * @module moodle-atto_alignjustify-button
 */

/**
 * Atto text editor align justify plugin.
 *
 * @namespace M.atto_alignjustify
 * @class button
 * @extends M.editor_atto.EditorPlugin
 */

Y.namespace('M.atto_alignjustify').Button = Y.Base.create('button', Y.M.editor_atto.EditorPlugin, [], {
    initializer: function() {
        var alignment;

        alignment = 'justifyFull';
        this.addButton({
            icon: 'icon',
			iconComponent: 'atto_alignjustify'
            title: 'justify',
            buttonName: alignment,
            callback: this._changeStyle,
            callbackArgs: alignment
        });

    },


    /**
     * Change the alignment to the specified justification.
     *
     * @method _changeStyle
     * @param {EventFacade} e
     * @param {string} justification The execCommand for the new justification.
     * @private
     */
    _changeStyle: function(e, justification) {
        var host = this.get('host');

        // We temporarily re-enable CSS styling to try to have the most consistency.
        // Though, IE, as always, is stubborn and will do its own thing...
        host.enableCssStyling();

        document.execCommand(justification, false, null);

        // Re-disable the CSS styling after making the change.
        host.disableCssStyling();

        // Mark the text as having been updated.
        this.markUpdated();

        this.editor.focus();
    }
});
